using System;
using System.IO;
using System.Collections.Generic;
using Xunit;
using Yamlarm;

namespace YamlarmTests {

    #region ArgumentParserTests
    public class ArgumentParserTests {
        public const string configExampleFile = "configExample.yaml";

        static Dictionary<object, object> commandConfig;

        static ArgumentParserTests() {
            IMultiSerializer serilizer = new MultiSerializer();
            commandConfig = (Dictionary<object, object>)serilizer.DesializeFromFile(configExampleFile);
        }

        ArgumentsParserTester argumentParserTester;

        public ArgumentParserTests() {
            argumentParserTester = new ArgumentsParserTester(commandConfig);
        }

        #region Constructor tests
        [Fact]
        public void ArgumentsParser_ConstructorTest1() {
            Assert.Equal("yamlarm", argumentParserTester.Application);

            Assert.True(argumentParserTester.AvailableCommands.ContainsKey("convert"));
            Assert.NotNull(argumentParserTester.AvailableCommands["convert"]);
            Assert.True(argumentParserTester.AvailableCommands.ContainsKey("combine"));
            Assert.NotNull(argumentParserTester.AvailableCommands["combine"]);
            Assert.True(argumentParserTester.AvailableCommands.ContainsKey("deploy"));
            Assert.NotNull(argumentParserTester.AvailableCommands["deploy"]);

            Assert.True(argumentParserTester.AvailableParameters.ContainsKey("--recurse"));
            Assert.NotNull(argumentParserTester.AvailableParameters["--recurse"]);
            Assert.True(argumentParserTester.AvailableParameters.ContainsKey("--debug"));
            Assert.NotNull(argumentParserTester.AvailableParameters["--debug"]);
        }
        #endregion (Constructor tests)

        #region IsHelpArgument
        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_ValidHelpArgument() {
            string arg = "--help";
            Assert.True(argumentParserTester.IsHelpArgument(arg));
        }

        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_ValidHelpArgumentUpper() {
            string arg = "--hElP";
            Assert.True(argumentParserTester.IsHelpArgument(arg));
        }

        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_ValidHelpShortcut() {
            string arg = "-h";
            Assert.True(argumentParserTester.IsHelpArgument(arg));
        }

        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_ValidHelpShortcutUpper() {
            string arg = "-H";
            Assert.True(argumentParserTester.IsHelpArgument(arg));
        }

        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_InvalidHelpArgument() {
            string arg = "-help";
            Assert.False(argumentParserTester.IsHelpArgument(arg));
        }

        [Fact]
        public void ArgumentsParser_IsHelpArgumentTest_InvalidHelpShortcut() {
            string arg = "h";
            Assert.False(argumentParserTester.IsHelpArgument(arg));
        }
        #endregion (IsHelpArgument)

        #region CheckArgument
        [Fact]
        public void ArgumentsParser_CheckArgument_Length2Unparsed() {
            string arg = "-z";
            Assert.False(argumentParserTester.AvailableShortcuts.ContainsKey("-z"));
            Assert.Equal(ArgumentCheckResult.Unparsed, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("-z", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_LengthNot2UnavailableParameter() {
            string arg = "abc";
            Assert.False(argumentParserTester.AvailableParameters.ContainsKey("abc"));
            Assert.Equal(ArgumentCheckResult.Unparsed, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("abc", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_Flag() {
            AddAvailableParameter("someParam", "s", false);
            string arg = "--someParam";
            Assert.Equal(ArgumentCheckResult.Flag, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_FlagFromShotcut() {
            AddAvailableParameter("someParam", "s", false);
            string arg = "-s";
            Assert.Equal(ArgumentCheckResult.Flag, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_Parameter() {
            AddAvailableParameter("someParam", "s", true, true);
            string arg = "--someParam";
            Assert.Equal(ArgumentCheckResult.Parameter, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_ParameterFromShotcut() {
            AddAvailableParameter("someParam", "s", true, true);
            string arg = "-s";
            Assert.Equal(ArgumentCheckResult.Parameter, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_ParameterOrFlag() {
            AddAvailableParameter("someParam", "s", true, false);
            string arg = "--someParam";
            Assert.Equal(ArgumentCheckResult.ParameterOrFlag, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }

        [Fact]
        public void ArgumentsParser_CheckArgument_ParameterOrFlagFromShotcut() {
            AddAvailableParameter("someParam", "s", true, false);
            string arg = "-s";
            Assert.Equal(ArgumentCheckResult.ParameterOrFlag, argumentParserTester.CheckArgument(ref arg));
            Assert.Equal("--someParam", arg);
        }
        #endregion (CheckArgument)

        #region AddParsedFlag
        [Fact]
        public void ArgumentsParser_AddParsedFlag_SimpleCheck() {
            string flag = "--someFlag";
            argumentParserTester.AddParsedFlag(flag);
            Assert.True(argumentParserTester.ParsedParametersAndValues.ContainsKey("someFlag"));
            Assert.Equal(string.Empty, argumentParserTester.ParsedParametersAndValues["someFlag"]);
        }
        #endregion (AddParsedFlag)

        #region AddParsedParameter
        [Fact]
        public void ArgumentsParser_AddParsedParameter_SimpleCheck() {
            string parameter = "--someParam";
            string value = "someValue";
            argumentParserTester.AddParsedParameter(parameter, value);
            Assert.True(argumentParserTester.ParsedParametersAndValues.ContainsKey("someParam"));
            Assert.Equal(value, argumentParserTester.ParsedParametersAndValues["someParam"]);
        }
        #endregion (AddParsedParameter)

        #region AddAvailableParameters
        [Fact]
        public void ArgumentsParser_AddAvailableParameter_TwoParameters() {
            List<object> parameters = new List<object>(2);
            Dictionary<object, object> parameter1 = new Dictionary<object, object>();
            parameter1.Add("name", "param_a");
            parameter1.Add("shortcut", "-a");
            parameters.Add(parameter1);
            Dictionary<object, object> parameter2 = new Dictionary<object, object>();
            parameter2.Add("name", "param_b");
            parameter2.Add("shortcut", "-b");
            parameters.Add(parameter2);
            argumentParserTester.AddAvailableParameters(parameters);
            CheckAvailableParameter(parameter1);
            CheckAvailableParameter(parameter2);
        }
        #endregion (AddAvailableParameters)

        #region ParseZeroArguments
        [Fact]
        public void ArgumentsParser_ParseZeroArgumentsTest() {
            Assert.NotEqual(string.Empty, argumentParserTester.ParseZeroArguments());
        }
        #endregion (ParseZeroArguments)

        #region ParseSingeArgument
        [Fact]
        public void ArgumentsParser_ParseSingeArgumentTest1_ValidHelpParameter() {
            string[] args = new string[] { "--help" };
            Assert.Equal(string.Empty, argumentParserTester.ParseSingleArgument(args));
            CheckHelpArgument();
            CheckParsedCommand("yamlarm", false);
        }

        [Fact]
        public void ArgumentsParser_ParseSingeArgumentTest5_BadHelpParameter() {
            string[] args = new string[] { "-help" };
            Assert.NotEqual(string.Empty, argumentParserTester.ParseSingleArgument(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            Assert.Null(argumentParserTester.ParsedCommandObject);
        }
        #endregion (ParseZeroArguments)

        #region ParseTwoArguments
        [Fact]
        public void ArgumentsParser_ParseTwoArgumentsTest1_ValidCommandAndPath() {
            string path = "tmp";
            CheckFolderToExist(path);
            string[] args = new string[] { "combine", path };
            Assert.Equal(string.Empty, argumentParserTester.ParseTwoArguments(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            Assert.NotNull(argumentParserTester.ParsedPath);
            Assert.Equal(path, argumentParserTester.ParsedPath);
            CheckParsedCommand("combine");
        }

        [Fact]
        public void ArgumentsParser_ParseTwoArgumentsTest2_ValidCommandAndHelp() {
            string[] args = new string[] { "combine", "--help" };
            Assert.Equal(string.Empty, argumentParserTester.ParseTwoArguments(args));
            CheckHelpArgument();
            CheckParsedCommand("combine");
        }

        [Fact]
        public void ArgumentsParser_ParseTwoArgumentsTest3_ValidCommandInvalidOptionAndPath() {
            string[] args = new string[] { "combine", "-help" };
            Assert.NotEqual(string.Empty, argumentParserTester.ParseTwoArguments(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            CheckParsedCommand("combine");
        }

        [Fact]
        public void ArgumentsParser_ParseTwoArgumentsTest4_InvalidCommandValidOption() {
            string[] args = new string[] { "combin", "--help" };
            Assert.NotEqual(string.Empty, argumentParserTester.ParseTwoArguments(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            Assert.Null(argumentParserTester.ParsedCommandObject);
        }
        #endregion (ParseTwoArguments)

        #region ParseManyArguments
        [Fact]
        public void ArgumentsParser_ParseManyArgumentsTest_IncorrectCommand() {
            string[] args = new string[] { "combin", "--someFlag", "--help" };
            Assert.NotEqual(string.Empty, argumentParserTester.ParseManyArguments(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            Assert.Null(argumentParserTester.ParsedCommandObject);
        }

        [Fact]
        public void ArgumentsParser_ParseManyArgumentsTest_UnexistingPath() {
            string[] args = new string[] { "combine", "-r", ".\\surelyUnexistingPath" };
            Assert.NotEqual(string.Empty, argumentParserTester.ParseManyArguments(args));
            Assert.Empty(argumentParserTester.ParsedParametersAndValues);
            CheckParsedCommand("combine");
        }
        #endregion (ParseManyArguments)

        #region SupportMethods
        void CheckFolderToExist(string folder) {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }

        void CheckParsedCommand(string commandName, bool checkParsedCommandProperty = true) {
            Assert.NotNull(argumentParserTester.ParsedCommandObject);
            Dictionary<object, object> commandObject = argumentParserTester.ParsedCommandObject as Dictionary<object, object>;
            Assert.NotNull(commandObject);
            Assert.True(commandObject.ContainsKey("name"));
            Assert.Equal(commandName, commandObject["name"]);
            if (checkParsedCommandProperty)
                Assert.Equal(commandName, argumentParserTester.ParsedCommand);
        }

        void CheckHelpArgument() {
            Assert.Single(argumentParserTester.ParsedParametersAndValues);
            Assert.True(argumentParserTester.ParsedParametersAndValues.ContainsKey("help"));
            Assert.Equal(string.Empty, argumentParserTester.ParsedParametersAndValues["help"]);
        }

        void AddAvailableParameter(string name, string shortcut, bool hasValue = true, bool valueObligatory = true) {
            Dictionary<object, object> someParam = new Dictionary<object, object>();
            someParam.Add("name", name);
            someParam.Add("hasValue", hasValue ? "true" : "false");
            someParam.Add("valueObligatory", valueObligatory ? "true" : "false");
            argumentParserTester.AvailableParameters.Add($"--{name}", someParam);
            argumentParserTester.AvailableShortcuts.Add($"-{shortcut}", someParam);
        }

        void CheckAvailableParameter(Dictionary<object, object> parameter) {
            string fullName = "--" + parameter["name"].ToString().Trim().ToLower();
            Assert.Equal(parameter, argumentParserTester.AvailableParameters[fullName]);
            Assert.Equal(parameter, argumentParserTester.AvailableShortcuts[parameter["shortcut"].ToString()]);
        }
        #endregion (SupportMethods)
    }
    #endregion (ArgumentParserTests)
}
