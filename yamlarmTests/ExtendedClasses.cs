﻿using System;
using System.Collections.Generic;
using Xunit;
using Yamlarm;

namespace YamlarmTests {
    #region ArgumentsParserTester
    class ArgumentsParserTester : ArgumentsParser {
        public ArgumentsParserTester(Dictionary<object, object> commandConfig) : base(commandConfig) { }

        public string Application { get { return application; } }
        public Dictionary<object, object> CommandConfig { get { return commandConfig; } }
        public Dictionary<object, object> CommandObject { get { return commandObject; } }
        public Dictionary<string, Dictionary<object, object>> AvailableCommands { get { return availableCommands; } }
        public Dictionary<string, Dictionary<object, object>> AvailableParameters { get { return availableParameters; } }
        public Dictionary<string, Dictionary<object, object>> AvailableShortcuts { get { return availableShortcuts; } }

        new public string ParseZeroArguments() {
            return base.ParseZeroArguments();
        }

        new public string ParseSingleArgument(string[] args) {
            return base.ParseSingleArgument(args);
        }

        new public string ParseTwoArguments(string[] args) {
            return base.ParseTwoArguments(args);
        }

        new public string ParseManyArguments(string[] args) {
            return base.ParseManyArguments(args);
        }

        new public bool IsHelpArgument(string arg) {
            return base.IsHelpArgument(arg);
        }

        new public ArgumentCheckResult CheckArgument(ref string arg) {
            return base.CheckArgument(ref arg);
        }

        new public void AddParsedFlag(string fullFlag) {
            base.AddParsedFlag(fullFlag);
        }

        new public void AddParsedParameter(string fullParameter, string value) {
            base.AddParsedParameter(fullParameter, value);
        }

        new public void AddAvailableParameters(object parameters) {
            base.AddAvailableParameters(parameters);
        }
    }
    #endregion (ArgumentsParserTester)

    #region MultiSerializerTester
    public class MultiSerializerTester : MultiSerializer {
        public MultiSerializerTester() : base() { }

        new public void SerializeToYamlFile(object theObject, string fileName) {
            base.SerializeToYamlFile(theObject, fileName);
        }

        new public object DeserializeYamlFile(string fileName) {
            return base.DeserializeYamlFile(fileName);
        }

        new public object DeserializeJsonFile(string fileName) {
            return base.DeserializeJsonFile(fileName);
        }

        new public void SerializeToJsonFile(object theObject, string fileName) {
            base.SerializeToJsonFile(theObject, fileName);
        }
    }
    #endregion (MultiSerializerTester)
}