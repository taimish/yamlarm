﻿using System;
using System.IO;
using System.Collections.Generic;
using Xunit;

namespace YamlarmTests {
    public class MultiSerializerTests {
        public const string tmpExamplesFolder = "tmp";
        public const string jsonExample1FileName = "jsonExample1.json";
        public const string jsonExample2FileName = "jsonExample2.json";
        public const string yamlExample1FileName = "yamlExample1.yaml";
        public const string yamlExample2FileName = "yamlExample2.yaml";
        MultiSerializerTester multiSerializerTester;

        public MultiSerializerTests() {
            multiSerializerTester = new MultiSerializerTester();
        }

        #region YAML
        [Fact]
        public void MultiSerializer_SerializeToYamlFile1_RootDictionary() {
            CheckFolderToExist(tmpExamplesFolder);
            string tmpFileName = Path.Combine(tmpExamplesFolder, "example1Tmp.yaml");
            DeleteFileIfExist(tmpFileName);
            Dictionary<string, object> objectToSerialize = GenerateObjectForExample1();
            multiSerializerTester.SerializeToYamlFile(objectToSerialize, tmpFileName);
            string serializationResult = File.ReadAllText(tmpFileName).Trim();
            string serializationSample = File.ReadAllText(yamlExample1FileName).Trim();
            Assert.Equal(serializationSample, serializationResult);
        }

        [Fact]
        public void MultiSerializer_SerializeToYamlFile2_RootArray() {
            CheckFolderToExist(tmpExamplesFolder);
            string tmpFileName = Path.Combine(tmpExamplesFolder, "example2Tmp.yaml");
            DeleteFileIfExist(tmpFileName);
            List<object> objectToSerialize = GenerateObjectForExample2();
            multiSerializerTester.SerializeToYamlFile(objectToSerialize, tmpFileName);
            string serializationResult = File.ReadAllText(tmpFileName).Trim();
            string serializationSample = File.ReadAllText(yamlExample2FileName).Trim();
            Assert.Equal(serializationSample, serializationResult);
        }

        [Fact]
        public void MultiSerializer_DeserializeYamlFile1_RootDictionary() {
            object result = multiSerializerTester.DeserializeYamlFile(yamlExample1FileName);
            CheckObjectFromExample1(result);
        }

        [Fact]
        public void MultiSerializer_DeserializeYamlFile2_RootArray() {
            object result = multiSerializerTester.DeserializeYamlFile(yamlExample2FileName);
            CheckObjectFromExample2(result);
        }
        #endregion (YAML)

        #region JSON
        [Fact]
        public void MultiSerializer_SerializeToJsonFile1_RootDictionary() {
            CheckFolderToExist(tmpExamplesFolder);
            string tmpFileName = Path.Combine(tmpExamplesFolder, "example1Tmp.json");
            DeleteFileIfExist(tmpFileName);
            Dictionary<string, object> objectToSerialize = GenerateObjectForExample1();
            multiSerializerTester.SerializeToJsonFile(objectToSerialize, tmpFileName);
            string serializationResult = File.ReadAllText(tmpFileName).Trim();
            string serializationSample = File.ReadAllText(jsonExample1FileName).Trim();
            Assert.Equal(serializationSample, serializationResult);
        }

        [Fact]
        public void MultiSerializer_SerializeToJsonFile2_RootArray() {
            CheckFolderToExist(tmpExamplesFolder);
            string tmpFileName = Path.Combine(tmpExamplesFolder, "example2Tmp.json");
            DeleteFileIfExist(tmpFileName);
            List<object> objectToSerialize = GenerateObjectForExample2();
            multiSerializerTester.SerializeToJsonFile(objectToSerialize, tmpFileName);
            string serializationResult = File.ReadAllText(tmpFileName).Trim();
            string serializationSample = File.ReadAllText(jsonExample2FileName).Trim();
            Assert.Equal(serializationSample, serializationResult);
        }

        [Fact]
        public void MultiSerializer_DeserializeJsonFile1_RootDictionary() {
            object result = multiSerializerTester.DeserializeJsonFile(jsonExample1FileName);
            CheckObjectFromExample1(result);
        }

        [Fact]
        public void MultiSerializer_DeserializeJsonFile2_RootArray() {
            object result = multiSerializerTester.DeserializeJsonFile(jsonExample2FileName);
            CheckObjectFromExample2(result);
        }
        #endregion (JSON)

        #region Main

        [Fact]
        public void MultiSerializer_DeserializeFromFile_JsonExtension() {
            object result = multiSerializerTester.DesializeFromFile(jsonExample1FileName);
            Assert.NotNull(result);
        }

        [Fact]
        public void MultiSerializer_DeserializeFromFile_YamlExtension() {
            object result = multiSerializerTester.DesializeFromFile(yamlExample1FileName);
            Assert.NotNull(result);
        }

        [Fact]
        public void MultiSerializer_DeserializeFromFile_UnknownExtension() {
            Assert.Throws<InvalidOperationException>(() => { multiSerializerTester.DesializeFromFile("file_with_unsupported_extension.mp3"); });
        }

        #endregion (Main)

        #region ObjectCheck
        void CheckObjectFromExample1(object objectToCheck) {
            Dictionary<object, object> rootObject = CheckDictionary(objectToCheck, new string[] { "name1", "name2", "int1", "date1", "array1", "name5" });
            Assert.Equal("value1", rootObject["name1"].ToString());
            Assert.Equal("value2", rootObject["name2"].ToString());
            Assert.Equal("13", rootObject["int1"].ToString());
            Assert.Equal("2020-11-01", rootObject["date1"]);

            List<object> array1 = CheckArray(rootObject["array1"], 3);
            Assert.Equal("item4", array1[1].ToString());

            Dictionary<object, object> subobject1 = CheckDictionary(array1[0], new string[] { "name3", "array2" });
            Assert.Equal("value3", subobject1["name3"].ToString());

            List<object> array2 = CheckArray(subobject1["array2"], 3);
            Assert.Equal("item1", array2[0].ToString());
            Assert.Equal("item2", array2[1].ToString());
            Assert.Equal("item3", array2[2].ToString());

            Dictionary<object, object> subobject2 = CheckDictionary(array1[2], new string[] { "name4" });
            Assert.Equal("value4", subobject2["name4"].ToString());

            Dictionary<object, object> subobject3 = CheckDictionary(rootObject["name5"], new string[] { "name6" });
            Assert.Equal("value6", subobject3["name6"].ToString());
        }

        void CheckObjectFromExample2(object objectToCheck) {
            List<object> rootArray = CheckArray(objectToCheck, 2);

            Dictionary<object, object> subobject1 = CheckDictionary(rootArray[0], new string[] { "name1", "name2", "array1" });
            Assert.Equal("value1", subobject1["name1"].ToString());
            Assert.Equal("value2", subobject1["name2"].ToString());

            List<object> array1 = CheckArray(subobject1["array1"], 2);

            Dictionary<object, object> subobject2 = CheckDictionary(array1[0], new string[] { "name3", "array2" });
            Assert.Equal("value3", subobject2["name3"].ToString());

            List<object> array2 = CheckArray(subobject2["array2"], 3);
            Assert.Equal("item1", array2[0].ToString());
            Assert.Equal("item2", array2[1].ToString());
            Assert.Equal("item3", array2[2].ToString());

            Dictionary<object, object> subobject3 = CheckDictionary(array1[1], new string[] { "item4" });

            Dictionary<object, object> subobject4 = CheckDictionary(subobject3["item4"], new string[] { "name4" });
            Assert.Equal("value4", subobject4["name4"].ToString());

            Dictionary<object, object> subobject5 = CheckDictionary(rootArray[1], new string[] { "name5", "name7" });
            Assert.Equal("value7", subobject5["name7"].ToString());

            Dictionary<object, object> subobject6 = CheckDictionary(subobject5["name5"], new string[] { "name6" });
            Assert.Equal("value6", subobject6["name6"].ToString());
        }
        #endregion (ObjectCheck)

        #region SupportMethods
        void DeleteFileIfExist(string fileName) {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }

        void CheckFolderToExist(string folder) {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }

        Dictionary<string, object> GenerateObjectForExample1() {
            Dictionary<string, object> resultObject = new Dictionary<string, object>();
            resultObject.Add("name1", "value1");
            resultObject.Add("name2", "value2");
            resultObject.Add("int1", 13);
            resultObject.Add("date1", "2020-11-01");
            List<object> array1 = new List<object>(3);
            resultObject.Add("array1", array1);
            Dictionary<string, object> subobject1 = new Dictionary<string, object>();
            array1.Add(subobject1);
            subobject1.Add("name3", "value3");
            List<object> array2 = new List<object>(3);
            subobject1.Add("array2", array2);
            array2.Add("item1");
            array2.Add("item2");
            array2.Add("item3");
            array1.Add("item4");
            Dictionary<string, object> subobject2 = new Dictionary<string, object>();
            array1.Add(subobject2);
            subobject2.Add("name4", "value4");
            Dictionary<string, object> subobject3 = new Dictionary<string, object>();
            resultObject.Add("name5", subobject3);
            subobject3.Add("name6", "value6");
            return resultObject;
        }

        List<object> GenerateObjectForExample2() {
            List<object> resultObject = new List<object>(2);
            Dictionary<string, object> subobject1 = new Dictionary<string, object>();
            resultObject.Add(subobject1);
            subobject1.Add("name1", "value1");
            subobject1.Add("name2", "value2");
            List<object> array1 = new List<object>(3);
            subobject1.Add("array1", array1);
            Dictionary<string, object> subobject2 = new Dictionary<string, object>();
            array1.Add(subobject2);
            subobject2.Add("name3", "value3");
            List<object> array2 = new List<object>(3);
            subobject2.Add("array2", array2);
            array2.Add("item1");
            array2.Add("item2");
            array2.Add("item3");
            Dictionary<string, object> subobject3 = new Dictionary<string, object>();
            array1.Add(subobject3);
            Dictionary<string, object> subobject4 = new Dictionary<string, object>();
            subobject3.Add("item4", subobject4);
            subobject4.Add("name4", "value4");
            Dictionary<string, object> subobject5 = new Dictionary<string, object>();
            resultObject.Add(subobject5);
            Dictionary<string, object> subobject6 = new Dictionary<string, object>();
            subobject5.Add("name5", subobject6);
            subobject6.Add("name6", "value6");
            subobject5.Add("name7", "value7");
            return resultObject;
        }

        Dictionary<object, object> CheckDictionary(object objectToCheck, string[] keys) {
            Dictionary<object, object> dictionary = objectToCheck as Dictionary<object, object>;
            Assert.NotNull(dictionary);
            foreach (string key in keys)
                Assert.True(dictionary.ContainsKey(key));
            return dictionary;
        }

        List<object> CheckArray(object objectToCheck, int count) {
            List<object> array = objectToCheck as List<object>;
            Assert.NotNull(array);
            Assert.Equal(count, array.Count);
            return array;
        }
        #endregion (SupportMethods)
    }
}
