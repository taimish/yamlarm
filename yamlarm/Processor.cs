﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Yamlarm {
    public class Processor {
        public const string CommandsConfigFile = "commands.yaml";
        public const string RecurseParameter = "recurse";
        public const string DebugParameter = "debug";
        string configFullFileName;

        public Processor() {
            Console.WriteLine("Yamlarm CLI tool");
            MultiSerializer = new MultiSerializer();
            configFullFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CommandsConfigFile);
            CommandConfig = (Dictionary<object, object>)MultiSerializer.DesializeFromFile(configFullFileName);
            ArgumentParser = new ArgumentsParser(CommandConfig);
        }

        public Dictionary<object, object> CommandConfig { get; private set; }
        public IArgumentsParser ArgumentParser { get; private set; }
        public IMultiSerializer MultiSerializer { get; private set; }
        public bool Recurse { get; private set; }
        public bool Debug { get; private set; }

        public void Process(string[] args) {
            ParseArguments(args);

            PrintHelpIfNeeded();

            RunCommand();
        }

        void ParseArguments(string[] args) {
            string parserResult = ArgumentParser.ParseArgumentsReturnSuccess(args);
            if (parserResult != string.Empty) {
                Console.Error.WriteLine(parserResult + "\n");
                Environment.Exit(1);
            }

            Recurse = ArgumentParser.ParsedParametersAndValues.ContainsKey(RecurseParameter);
            Debug = ArgumentParser.ParsedParametersAndValues.ContainsKey(DebugParameter);
            if (Debug)
                Console.WriteLine($"   [Debug] Config file name: {configFullFileName}");
        }

        void PrintHelpIfNeeded() {
            if (ArgumentParser.ParsedParametersAndValues.ContainsKey(ArgumentsParser.HelpRequests[0].Substring(2))) {
                HelpPrinter helpPrinter = new HelpPrinter(CommandConfig);
                helpPrinter.DisplayCommandHelp(ArgumentParser.ParsedCommandObject);
                Environment.Exit(0);
            }
        }

        void RunCommand() {
            if (string.IsNullOrEmpty(ArgumentParser.ParsedCommand))
                throw new InvalidOperationException($"[Error] Error checking parameters - no command parsed.");

            string result = null;
            switch (ArgumentParser.ParsedCommand) {
                case "convert":
                    Converter converter = new Converter(MultiSerializer, ArgumentParser, Recurse, Debug);
                    result = converter.Convert();
                    break;
                case "combine":
                    Combiner combiner = new Combiner(MultiSerializer, ArgumentParser, Recurse, Debug);
                    result = combiner.Combine();
                    break;
                case "deploy":
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrEmpty(result)) {
                Console.Error.WriteLine(result);
                Environment.Exit(1);
            }
        }
    }
}
