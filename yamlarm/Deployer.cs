﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Yamlarm {
    public class Deployer {
        const string outputParameter = "output";
        const string subscriptionParameter = "subscription";
        const string resourceGroupParameter = "resourcegroup";
        const string parametersParameter = "parameters";
        const string validateParameter = "validate";
        const string modeParameter = "mode";
        IMultiSerializer multiSerializer;
        IArgumentsParser argumentParser;
        bool recurse;
        bool debug;
        string output;
        string subscription;
        string resourceGroup;
        string templateParametersStr;
        string validate;
        string mode;
        string command;
        string logFullFileName;
        List<string> templateParameters;

        public Deployer(IMultiSerializer multiSerializer, IArgumentsParser argumentParser, bool recurse, bool debug) {
            this.multiSerializer = multiSerializer;
            this.argumentParser = argumentParser;
            this.recurse = recurse;
            this.debug = debug;
        }

        Dictionary<string, string> ParsedParameters { get { return argumentParser.ParsedParametersAndValues; } }
        string ParsedPath { get { return argumentParser.ParsedPath; } }
        List<string> SupportedFormats { get { return multiSerializer.SupportedFormats; } }
        bool ValidateOnly { get { return validate != null && (validate == "true" || validate == string.Empty); } }

        public string Deploy() {

            return string.Empty;
        }

        string CheckSpecificParameters() {
            // Output - folder where to store converted files and deployment logs, default is the 'deployment_info' subfolder of the path value
            if (!ParsedParameters.TryGetValue(outputParameter, out output))
                output = Path.Combine(ParsedPath, "deployment_info");
            if (debug)
                Console.WriteLine($"   [Debug] Output folder: [{output}]");

            // Subscription
            if (ParsedParameters.TryGetValue(subscriptionParameter, out subscription)) {
                if (debug)
                    Console.WriteLine($"   [Debug] Subscription to use: [{subscription}]");
            }
            else if (debug)
                Console.WriteLine($"   [Debug] Using default Azure CLI subscription");

            // Resource group
            if (ParsedParameters.TryGetValue(resourceGroupParameter, out resourceGroup)) {
                if (debug)
                    Console.WriteLine($"   [Debug] Resource group to use: [{resourceGroup}]");
            }
            else if (debug)
                Console.WriteLine($"   [Debug] Using default Azure CLI resource group");

            // Parameters
            if (ParsedParameters.TryGetValue(parametersParameter, out templateParametersStr)) {
                string[] templateParametersStrs = templateParametersStr.Split(';', StringSplitOptions.RemoveEmptyEntries);
                templateParameters = new List<string>(templateParametersStrs.Length);
                foreach (string parameterPair in templateParametersStrs) {
                    string result = ValidateParameterPair(parameterPair);
                    if (!string.IsNullOrEmpty(result))
                        return result;
                    templateParameters.Add(parameterPair.Trim());
                }
                if (debug)
                    Console.WriteLine($"   [Debug] Specified template parameters: [{templateParametersStr}], " +
                        $"total count: [{templateParameters.Count}]");
            }
            else if (debug)
                Console.WriteLine($"   [Debug] No template parameters specified");

            // Validate
            if (ParsedParameters.TryGetValue(validateParameter, out validate)) {
                if (validate != string.Empty || validate.ToLower() != "true" || validate.ToLower() != "false")
                    return $"[Error] Error parsing 'validate' parameter of the 'deploy' command - unknown value '{validate}', " +
                        $"valid value (if specified) is 'true' or 'false'";
            }
            if (debug)
                Console.WriteLine($"   [Debug] Validate: [{ValidateOnly}]");

            // Mode
            if (ParsedParameters.TryGetValue(modeParameter, out mode)) {
                if (mode.ToLower() != "incremental" || validate.ToLower() != "сomplete")
                    return $"[Error] Error parsing 'mode' parameter of the 'deploy' command - unknown value '{mode}', " +
                        $"valid value is 'incremental' or 'сomplete'";
            }
            else
                mode = "incremental";
            if (debug)
                Console.WriteLine($"   [Debug] Deployment mode: [{mode}]");

            command = ValidateOnly ? "validate" : "create";
            logFullFileName = Path.Combine(output, $"{command}_{DateTime.Now.ToShortDateString()}-{DateTime.Now.ToShortTimeString()}.log");

            return string.Empty;
        }

        string ValidateParameterPair(string parameterPair) {
            return string.Empty;
        }
    }
}
