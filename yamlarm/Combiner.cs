﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Yamlarm {
    public class Combiner {
        const string outputParameter = "output";
        const string defaultOutput = "deployment_info";
        const string resultFileName = "combined_template.json";
        IMultiSerializer multiSerializer;
        IArgumentsParser argumentParser;
        bool recurse;
        bool debug;
        string output;
        string mainObjectFileName;
        Dictionary<string, object> mainObject;
        List<object> resourceObjects;

        public Combiner(IMultiSerializer multiSerializer, IArgumentsParser argumentParser, bool recurse, bool debug) {
            this.multiSerializer = multiSerializer;
            this.argumentParser = argumentParser;
            this.recurse = recurse;
            this.debug = debug;
        }

        Dictionary<string, string> ParsedParameters { get { return argumentParser.ParsedParametersAndValues; } }
        string ParsedPath { get { return argumentParser.ParsedPath; } }
        List<string> SupportedFormats { get { return multiSerializer.SupportedFormats; } }

        public string Combine() {
            CheckSpecificParameters();

            string result = ProcessFiles();
            if (!string.IsNullOrEmpty(result))
                return result;

            result = CreateTemplate();
            if (!string.IsNullOrEmpty(result))
                return result;

            return string.Empty;
        }

        void CheckSpecificParameters() {
            // Output - folder where to store converted files, default is the path value
            if (!ParsedParameters.TryGetValue(outputParameter, out output))
                output = Path.Combine(ParsedPath, defaultOutput);
        }

        string ProcessFiles() {
            string message = $"\n-> Combining files...\n" +
                $"   - from path: {ParsedPath}\n" +
                $"   - output path: {output}";
            Console.WriteLine(message);
            List<string> filesToProcess = new List<string>();
            AddFilesRecursively(ParsedPath, filesToProcess, 1);
            if (filesToProcess.Count > 0)
                if (!Directory.Exists(output)) {
                    if (debug)
                        Console.WriteLine($"   [Debug] Output path {output} does not exist, creating it");
                    Directory.CreateDirectory(output);
                }
            resourceObjects = new List<object>();
            Console.WriteLine($"\n-> Processing files and resources:");
            foreach (string fileToProcess in filesToProcess) {
                string result = ProcessFile(fileToProcess);
                if (!string.IsNullOrEmpty(result))
                    return result;
            }
            return string.Empty;
        }

        string CreateTemplate() {
            if (mainObject["Resources"] != null) {
                IList<object> resourceList = mainObject["Resources"] as IList<object>;
                if (resourceList == null)
                    return $"\n[Error] The file with name '{mainObjectFileName}' contains an object that was considered to be the main one but its root property 'Resource' is not a list";
                foreach (object resourceObject in resourceObjects)
                    resourceList.Add(resourceObject);
            }
            else
                mainObject["Resources"] = resourceObjects;
            if (((List<object>)mainObject["Resources"]).Count> 0) {
                if (!Directory.Exists(output)) {
                    if (debug)
                        Console.WriteLine($"   [Debug] Output path {output} does not exist, creating it");
                    Directory.CreateDirectory(output);
                }
            }
            multiSerializer.SerializeToFile(mainObject, "json", Path.Combine(output, resultFileName));
            return string.Empty;
        }

        void AddFilesRecursively(string path, List<string> filesToProcess, int depth) {
            string[] files = Directory.GetFiles(path);
            if (debug)
                Console.WriteLine($"{new String(' ', depth * 2)}- Scanning folder: {path}");
            foreach (string fullFileName in files) {
                string extension = Path.GetExtension(fullFileName).Substring(1);
                if (SupportedFormats.Contains(extension)) {
                    filesToProcess.Add(fullFileName);
                    if (debug)
                        Console.WriteLine($"{new String(' ', depth * 2)}  - Added file: {fullFileName}");
                }
            }
            if (recurse)
                foreach (string subdirectory in Directory.GetDirectories(path))
                    AddFilesRecursively(subdirectory, filesToProcess, depth + 1);
        }

        string ProcessFile(string fullFileName) {
            object fileObject = null;
            try {
                fileObject = multiSerializer.DesializeFromFile(fullFileName);
            }
            catch (Exception e) {
                return $"[Error] Error deserializing the file with name '{fullFileName}': {e.Message}";
            }
            if (fileObject is IList<object>)
                foreach (var subobjects in (IList<object>)fileObject) {
                    Dictionary<string, object> resourceObject = subobjects as Dictionary<string, object>;
                    if (resourceObject == null)
                        return $"\n[Error] The file with name '{fullFileName}' contains list of objects where one of them - '{subobjects}' - has invalid syntax and is not an object of the '{Path.GetExtension(fullFileName).Substring(1)}' format";
                    CheckObjectToBeResource(resourceObject, fullFileName);
                }
            else {
                Dictionary<string, object> resourceObject = fileObject as Dictionary<string, object>;
                if (resourceObject == null)
                    return $"\n[Error] The file with name '{fullFileName}' contains an object '{fileObject}' that has invalid syntax and is not an object of the '{Path.GetExtension(fullFileName).Substring(1)}' format";
                if (resourceObject.ContainsKey("$schema") && resourceObject.ContainsKey("Resources")) {
                    mainObject = resourceObject;
                    mainObjectFileName = fullFileName;
                    Console.WriteLine($"   - File '{fullFileName}' is considered to be the main file");
                    if (debug)
                        Console.WriteLine($"     [Debug] File object {fileObject} from file with name [{fullFileName}] is considered to be the 'main' object as it contains properties with names '$schema' and 'Resources'");
                }
                else 
                    CheckObjectToBeResource(resourceObject, fullFileName);
            }
            return string.Empty;
        }

        bool CheckObjectToBeResource(Dictionary<string, object> objectToCheck, string fileName) {
            if (objectToCheck.ContainsKey("Type") || objectToCheck.ContainsKey("apiVersion")) {
                resourceObjects.Add(objectToCheck);
                if (debug)
                    Console.WriteLine($"     [Debug] File object or subelement {objectToCheck} from the file with name [{fileName}] is added to the resources list as it contains properties with names 'Type' and 'apiVersion'");
                Console.WriteLine($"   - Resource with type '{objectToCheck["Type"]}' is added from file '{fileName}'");
                return true;
            }
            else {
                if (debug)
                    Console.WriteLine($"     [Debug] File object or subelement {objectToCheck} from the file with name [{fileName}] is not added to the resources list as it does not contains both properties with names 'Type' and 'apiVersion'");
                return false;
            }
        }
    }
}
