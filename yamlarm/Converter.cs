﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Yamlarm {
    public class Converter {
        const string formatParameter = "format";
        const string moveParameter = "move";
        const string outputParameter = "output";
        IMultiSerializer multiSerializer;
        IArgumentsParser argumentParser;
        bool recurse;
        bool debug;
        string format;
        string move;
        string output;

        public Converter(IMultiSerializer multiSerializer, IArgumentsParser argumentParser, bool recurse, bool debug) {
            this.multiSerializer = multiSerializer;
            this.argumentParser = argumentParser;
            this.recurse = recurse;
            this.debug = debug;
        }

        Dictionary<string, string> ParsedParameters { get { return argumentParser.ParsedParametersAndValues; } }
        string ParsedPath { get { return argumentParser.ParsedPath; } }
        List<string> SupportedFormats { get { return multiSerializer.SupportedFormats; } }
        bool ShouldMove { get { return move != null; } }

        public string Convert() {
            string result = CheckSpecificParameters();
            if (!string.IsNullOrEmpty(result))
                return result;

            ProcessFiles();

            return string.Empty;
        }

        string CheckSpecificParameters() {
            // Format - format to which to convert all other supported formats to
            if (!ParsedParameters.TryGetValue(formatParameter, out format) || format == "yml")
                format = "yaml";
            if (!SupportedFormats.Contains(format))
                return $"[Error] Error converting files - specified format '{format}' is not supported; " +
                    $"Supported formats are: {string.Join(", ", SupportedFormats)}";

            // Move - subfolder of path where to move all source files that were converted
            move = null;  // No move is needed
            ParsedParameters.TryGetValue(moveParameter, out move);
            if (ShouldMove)
                if (move == string.Empty)
                    move = Path.Combine(ParsedPath, "before_conversion");  // Default move subfolder
                else if (!Path.IsPathRooted(move))
                    move = Path.Combine(ParsedPath, move);  // Subfolder of the path folder

            // Output - folder where to store converted files, default is the path value
            if (!ParsedParameters.TryGetValue(outputParameter, out output))
                output = ParsedPath;
            return string.Empty;
        }

        void ProcessFiles() {
            List<string> availableFormats = new List<string>(SupportedFormats);
            availableFormats.Remove(format);
            string message = $"\n-> Converting files...\n" +
                $"   - from path: {ParsedPath}\n" +
                $"   - with formats: {string.Join(", ", availableFormats)}\n" +
                $"   - to format: {format}\n" +
                $"   - output path: {output}";
            if (ShouldMove)
                message += $"\n   - moving sources to: {move}";
            Console.WriteLine(message);
            List<string> filesToProcess = new List<string>();
            AddFilesRecursively(ParsedPath, filesToProcess, availableFormats, 1);
            if (filesToProcess.Count > 0) {
                if (!Directory.Exists(output)) {
                    if (debug)
                        Console.WriteLine($"   [Debug] Output path {output} does not exist, creating it");
                    Directory.CreateDirectory(output);
                }
                if (ShouldMove && !Directory.Exists(move)) {
                    if (debug)
                        Console.WriteLine($"   [Debug] Move path {move} does not exist, creating it");
                    Directory.CreateDirectory(move);
                }
            }
            foreach (string fileToProcess in filesToProcess)
                ProcessFile(fileToProcess);
        }

        void AddFilesRecursively(string path, List<string> filesToProcess, List<string> availableFormats, int depth) {
            string[] files = Directory.GetFiles(path);
            if (debug)
                Console.WriteLine($"{new String(' ', depth * 2)}- Scanning folder: {path}");
            foreach (string fullFileName in files) {
                string extension = Path.GetExtension(fullFileName).Substring(1);
                if (availableFormats.Contains(extension)) {
                    filesToProcess.Add(fullFileName);
                    if (debug)
                        Console.WriteLine($"{new String(' ', depth * 2)}  - Added file: {fullFileName}");
                }
            }
            if (recurse)
                foreach (string subdirectory in Directory.GetDirectories(path))
                    AddFilesRecursively(subdirectory, filesToProcess, availableFormats, depth + 1);
        }

        void ProcessFile(string fullFileName) {
            object fileObject = multiSerializer.DesializeFromFile(fullFileName);
            string convertedFullFileName = Path.Combine(output, $"{Path.GetFileNameWithoutExtension(fullFileName)}.{format}");
            if (debug)
                Console.WriteLine($"   [Debug] Converting file: {fullFileName}" +
                    $"  To file: {convertedFullFileName}");
            multiSerializer.SerializeToFile(fileObject, format, convertedFullFileName);
            if (ShouldMove) {
                string fileName = Path.GetFileName(fullFileName);
                string movedFullFileName = Path.Combine(move, fileName);
                Directory.Move(fullFileName, movedFullFileName);
                if (debug)
                    Console.WriteLine($"   [Debug] Moving file to: {movedFullFileName}");
            }
            if (debug)
                Console.WriteLine($"   [Debug] Conversion complete");
        }
    }
}
