﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace Yamlarm {
    #region IArgumentsParser
    public interface IArgumentsParser {
        Dictionary<string, string> ParsedParametersAndValues { get; }
        public Dictionary<object, object> ParsedCommandObject { get; }
        public string ParsedCommand { get; }
        public string ParsedPath { get; }

        public string ParseArgumentsReturnSuccess(string[] args);
    }
    #endregion (ArgumentsParser)

    #region ArgumentsParser
    public class ArgumentsParser : IArgumentsParser {
        public static List<string> HelpRequests = new List<string>(new string[2] { "--help", "-h" });

        protected string application;
        protected Dictionary<object, object> commandConfig;
        protected Dictionary<object, object> commandObject;
        protected Dictionary<string, Dictionary<object, object>> availableCommands;
        protected Dictionary<string, Dictionary<object, object>> availableParameters;
        protected Dictionary<string, Dictionary<object, object>> availableShortcuts;

        public ArgumentsParser(Dictionary<object, object> commandConfig) {
            // Initializing
            availableCommands = new Dictionary<string, Dictionary<object, object>>();
            availableParameters = new Dictionary<string, Dictionary<object, object>>();
            availableShortcuts = new Dictionary<string, Dictionary<object, object>>();
            ParsedParametersAndValues = new Dictionary<string, string>();
            ParsedCommand = string.Empty;

            // Get commands
            this.commandConfig = commandConfig;
            application = commandConfig["application"].ToString();
            foreach (Dictionary<object, object> command in FormatHelper.IterateDictionaries(commandConfig["commands"]))
                availableCommands.Add(command["name"].ToString(), command);

            // Get common parameters
            AddAvailableParameters(commandConfig["parameters"]);
        }

        public Dictionary<object, object> ParsedCommandObject { get { return commandObject; } }
        public string ParsedCommand { get; private set; }
        public string ParsedPath { get; private set; }
        public Dictionary<string, string> ParsedParametersAndValues { get; }

        public string ParseArgumentsReturnSuccess(string[] args) {
            int argsCount = args.Length;
            switch (argsCount) {
                case 0:
                    return ParseZeroArguments();
                case 1:
                    return ParseSingleArgument(args);
                case 2:
                    return ParseTwoArguments(args);
                default:
                    return ParseManyArguments(args);
            }
        }

        protected string ParseZeroArguments() {
            return "[ERROR] Error parsing argumets - there are no arguments. " +
                "Please use '--help' or '-h' argument to see available options";
        }

        protected string ParseSingleArgument(string[] args) {
            if (IsHelpArgument(args[0])) {
                AddParsedFlag(HelpRequests[0]);
                commandObject = commandConfig;
                return string.Empty;
            }
            else {
                return $"[ERROR] Error parsing argumets - invalid single argument [{args[0]}]. " +
                    "Please use '--help' or '-h' argument to see available options";
            }
        }

        protected string ParseTwoArguments(string[] args) {
            string commandArg = args[0].Trim().ToLower();
            string pathOrHelpArg = args[1];
            // Checking ARG1 to be a valid command
            if (availableCommands.TryGetValue(commandArg, out commandObject)) {
                ParsedCommand = commandArg;
                // Checking ARG2 to be a help request
                if (IsHelpArgument(pathOrHelpArg)) {
                    AddParsedFlag(HelpRequests[0]);
                    return string.Empty;
                }
                // Checking ARG2 to be an existing path
                if (Directory.Exists(pathOrHelpArg)) {
                    ParsedPath = pathOrHelpArg;
                    return string.Empty;
                }
                else
                    return $"[ERROR] Error parsing argumets - the path specified [{pathOrHelpArg}] " +
                        "does not exist. Please specify an existing folder or request help with '--help' or '-h' argument.";
            }
            else
                return $"[ERROR] Error parsing argumets - invalid first argument [{commandArg}]. " +
                    "Please use '--help' or '-h' argument to see available options";
        }

        protected string ParseManyArguments(string[] args) {
            // Checking first argument to be a command
            string commandArg = args[0].Trim().ToLower();
            if (availableCommands.TryGetValue(commandArg, out commandObject))
                ParsedCommand = commandArg;
            else
                return $"[ERROR] Error parsing argumets - invalid command name [{commandArg}]. " +
                    "Please use '--help' or '-h' argument to see available commands";

            // Checking last argument to be an existing path
            string pathArg = args[args.Length - 1];
            if (Directory.Exists(pathArg))
                ParsedPath = pathArg;
            else
                return $"[ERROR] Error parsing argumets - the path specified [{pathArg}] " +
                    "does not exist. Please specify an existing folder.";

            // Adding current command arguments to available
            AddAvailableParameters(commandObject["parameters"]);

            // Checking other arguments
            int lastNonPathArgumentIndex = args.Length - 2;
            for (int argIndex = 1; argIndex <= lastNonPathArgumentIndex; argIndex++) {
                string currentArg = args[argIndex].Trim().ToLower();
                string nextArg = args[argIndex + 1].Trim().ToLower();
                switch (CheckArgument(ref currentArg)) {
                    case ArgumentCheckResult.Unparsed:
                        return $"[ERROR] Error parsing argumets - argument [{currentArg}] " +
                            $"with index [{argIndex}] is not a valid flag or parameter of the command {ParsedCommand}. " +
                            $"Try 'yamlarm {ParsedCommand} --help' for help.";
                    case ArgumentCheckResult.Parameter:
                        if (argIndex == lastNonPathArgumentIndex)
                            return $"[ERROR] Error parsing argumets - the parameter [{currentArg}] " +
                                "is the argument before the last and obligatory argument - path - but should have " +
                                $"a value before the path argument. Try 'yamlarm {ParsedCommand} --help' for help.";
                        AddParsedParameter(currentArg, nextArg);
                        argIndex++;
                        break;
                    case ArgumentCheckResult.Flag:
                        AddParsedFlag(currentArg);
                        break;
                    case ArgumentCheckResult.ParameterOrFlag:
                        if (argIndex == lastNonPathArgumentIndex)
                            AddParsedFlag(currentArg);
                        else
                            if (CheckArgument(ref nextArg) != ArgumentCheckResult.Unparsed)
                            AddParsedFlag(currentArg);
                        else {
                            AddParsedParameter(currentArg, nextArg);
                            argIndex++;
                        }
                        break;
                }
            }
            return string.Empty;
        }

        protected bool IsHelpArgument(string arg) {
            return HelpRequests.Contains(arg.Trim().ToLower());
        }

        protected ArgumentCheckResult CheckArgument(ref string arg) {
            Dictionary<object, object> parameter;
            if (arg.Length == 2) {
                if (!availableShortcuts.TryGetValue(arg, out parameter))
                    return ArgumentCheckResult.Unparsed;
                arg = $"--{parameter["name"]}";
            }
            else if (!availableParameters.TryGetValue(arg, out parameter))
                return ArgumentCheckResult.Unparsed;
            string hasValue = parameter["hasValue"].ToString();
            if (hasValue == "false")
                return ArgumentCheckResult.Flag;
            string valueObligatory = parameter["valueObligatory"].ToString();
            if (valueObligatory == "true")
                return ArgumentCheckResult.Parameter;
            return ArgumentCheckResult.ParameterOrFlag;
        }

        #region Support methods
        protected void AddParsedFlag(string fullFlag) {
            ParsedParametersAndValues.Add(fullFlag.Substring(2), string.Empty);
        }

        protected void AddParsedParameter(string fullParameter, string value) {
            ParsedParametersAndValues.Add(fullParameter.Substring(2), value);
        }

        protected void AddAvailableParameters(object parameters) {
            foreach (Dictionary<object, object> parameter in FormatHelper.IterateDictionaries(parameters)) {
                string fullParameter = "--" + parameter["name"].ToString().Trim().ToLower();
                string fullShortcut = parameter["shortcut"].ToString().Trim().ToLower();
                availableParameters.Add(fullParameter, parameter);
                availableShortcuts.Add(fullShortcut, parameter);
            }
        }
        #endregion (Support methods)
    }
    #endregion (ArgumentsParser)

    #region Enums
    public enum ArgumentCheckResult {
        Unparsed = 0,
        Parameter = 1,
        Flag = 1 << 1,
        ParameterOrFlag = 1 << 2,
        Parsed = Parameter | Flag | ParameterOrFlag
    }
    #endregion (Enums)
}
