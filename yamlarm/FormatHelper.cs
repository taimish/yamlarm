﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Yamlarm {
    #region FormatHelper
    public static class FormatHelper {
        public static IEnumerable<Dictionary<object, object>> IterateDictionaries(object collection) {
            foreach (object item in (List<object>)collection)
                yield return (Dictionary<object, object>)item;
        }

        public static IEnumerable<string> IterateStrings(object collection) {
            foreach (object item in (List<object>)collection)
                yield return (string)item;
        }

        public static string FindSubobjectName(Dictionary<object, object> parentObject, string subobject) {
            foreach (object key in parentObject.Keys)
                if ((string)parentObject[key] == subobject)
                    return key.ToString();
            return string.Empty;
        }

    }
    #endregion (FormatHelper)
}
