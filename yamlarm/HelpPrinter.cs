﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yamlarm {
    public class HelpPrinter {
        const int ScreenWidthInChars = 80;
        const int MostLongItemGapToDescription = 3;

        Dictionary<object, object> commandConfig;

        public HelpPrinter(Dictionary<object, object> commandConfig) {
            this.commandConfig = commandConfig;
        }

        public void DisplayCommandHelp(Dictionary<object, object> commandObject) {
            PrintCommandName(commandObject);

            PrintCommandDescription(commandObject);

            PrintCommandSyntax(commandObject);

            if (commandObject.TryGetValue("commands", out object commands))
                PrintSubcommandDescriptions(commands);
            else {
                PrintCommandParameters(commandObject);
                PrintCommandExamples(commandObject);
            }

            Console.WriteLine(string.Empty);
        }

        void PrintCommandName(Dictionary<object, object> commandObject) {
            if (!commandObject.TryGetValue("name", out object name))
                throw new InvalidOperationException($"[Error] Error printing help for a command object {commandObject} " +
                    $"- it doesn't have a 'name' property");
            Console.WriteLine(name);
        }

        void PrintCommandDescription(Dictionary<object, object> commandObject) {
            if (!commandObject.TryGetValue("description", out object description))
                throw new InvalidOperationException($"[Error] Error printing help for a command object {commandObject} " +
                    $"- it doesn't have a 'description' property");
            Console.WriteLine(FormatStringByWidth(description.ToString()));
        }

        void PrintCommandSyntax(Dictionary<object, object> commandObject) {
            if (!commandObject.TryGetValue("syntax", out object syntax))
                throw new InvalidOperationException($"[Error] Error printing help for a command object {commandObject} " +
                    $"- it doesn't have a 'syntax' property");
            Console.WriteLine("\nSyntax:\n" + syntax);
        }

        void PrintSubcommandDescriptions(object commands) {
            // Getting maximum command name length
            int commandNameMaxLength = 0;
            foreach (Dictionary<object, object> subcommand in FormatHelper.IterateDictionaries(commands)) {
                if (!subcommand.TryGetValue("name", out object subcommandName))
                    throw new InvalidOperationException($"[Error] A command object {subcommand} " +
                        $"doesn't have a 'name' property");
                int currentCommandLength = subcommandName.ToString().Length;
                if (currentCommandLength > commandNameMaxLength)
                    commandNameMaxLength = currentCommandLength;
            }
            // Printing command names with descriptions formatted
            string shift = new string(' ', commandNameMaxLength + MostLongItemGapToDescription);
            foreach (Dictionary<object, object> subcommand in FormatHelper.IterateDictionaries(commands)) {
                if (!subcommand.TryGetValue("name", out object subcommandName))
                    throw new InvalidOperationException($"[Error] A command object {subcommand} " +
                        $"doesn't have a 'name' property");
                if (!subcommand.TryGetValue("description", out object subcommandDescription))
                    throw new InvalidOperationException($"[Error] A command object {subcommand["name"]} " +
                        $"doesn't have a 'description' property)");
                Console.WriteLine("\n" + FormatNameAndDescription(subcommandName.ToString(), subcommandDescription.ToString(), shift));
            }
        }

        void PrintCommandParameters(Dictionary<object, object> commandObject) {
            // Getting maximum command name length
            int headerMaxLength = 0;
            object commonParameters = null;
            if (commandConfig.TryGetValue("parameters", out commonParameters))
                foreach (Dictionary<object, object> commonParameter in FormatHelper.IterateDictionaries(commonParameters)) {
                    int currentHeaderLength = GetParameterHeader(commonParameter).Length;
                    if (currentHeaderLength > headerMaxLength)
                        headerMaxLength = currentHeaderLength;
                }
            object parameters = null;
            if (commandObject.TryGetValue("parameters", out parameters))
                foreach (Dictionary<object, object> commonParameter in FormatHelper.IterateDictionaries(parameters)) {
                    int currentHeaderLength = GetParameterHeader(commonParameter).Length;
                    if (currentHeaderLength > headerMaxLength)
                        headerMaxLength = currentHeaderLength;
                }
            // Printing command names with descriptions formatted
            string shift = new string(' ', headerMaxLength + MostLongItemGapToDescription);
            if (parameters != null) {
                Console.WriteLine("\nCommand-specific options:");
                foreach (Dictionary<object, object> parameter in FormatHelper.IterateDictionaries(parameters)) {
                    if (!parameter.TryGetValue("description", out object description))
                        throw new InvalidOperationException($"[Error] A parameter object '{parameter["name"]}' " +
                            $"doesn't have a 'description' property");
                    Console.WriteLine(FormatNameAndDescription(GetParameterHeader(parameter), description.ToString(), shift));
                }
            }
            if (commonParameters != null) {
                Console.WriteLine("\nCommon options:");
                foreach (Dictionary<object, object> commonParameter in FormatHelper.IterateDictionaries(commonParameters)) {
                    if (!commonParameter.TryGetValue("description", out object description))
                        throw new InvalidOperationException($"[Error] A parameter object '{commonParameter["name"]}' " +
                            $"doesn't have a 'description' property");
                    Console.WriteLine(FormatNameAndDescription(GetParameterHeader(commonParameter), description.ToString(), shift));
                }
            }
        }

        void PrintCommandExamples(Dictionary<object, object> commandObject) {
            if (!commandObject.TryGetValue("examples", out object examples))
                throw new InvalidOperationException($"[Error] Error printing help for a command object {commandObject["name"]} " +
                    $"- it doesn't have an 'examples' property");
            Console.WriteLine("\nExamples:");
            foreach (string example in FormatHelper.IterateStrings(examples))
                Console.WriteLine(FormatExampleByWidth(example));
        }

        #region Support methods
        string GetParameterHeader(Dictionary<object, object> parameterObject) {
            string result = string.Empty;
            if (!parameterObject.TryGetValue("name", out object name))
                throw new InvalidOperationException($"[Error] A parameter object '{parameterObject}' " +
                    $"doesn't have a 'name' property");
            if (!parameterObject.TryGetValue("shortcut", out object shortcut))
                throw new InvalidOperationException($"[Error] A parameter object '{parameterObject["name"]}' " +
                    $"doesn't have a 'shortcut' property");
            if (!parameterObject.TryGetValue("hasValue", out object hasValue))
                throw new InvalidOperationException($"[Error] A parameter object '{parameterObject["name"]}' " +
                    $"doesn't have a 'hasValue' property");
            if (hasValue.ToString() == "false")
                return $"{shortcut}, --{name}";
            else if (hasValue.ToString() == "true") {
                if (!parameterObject.TryGetValue("valueObligatory", out object valueObligatory))
                    throw new InvalidOperationException($"[Error] A parameter object '{parameterObject["name"]}' " +
                        $"doesn't have a 'valueObligatory' property although its 'hasValue' property is 'true'");
                string parameterValue = string.Empty;
                if (valueObligatory.ToString() == "true")
                    parameterValue = "string";
                else if (valueObligatory.ToString() == "false")
                    parameterValue = "[string]";
                else
                    throw new InvalidOperationException($"[Error] A parameter object '{parameterObject["name"]}' " +
                        $"has invalid 'valueObligatory' property value - '{valueObligatory}'. Valid are 'true' or 'false'");
                return $"{shortcut} {parameterValue}, --{name} {parameterValue}";
            }
            else
                throw new InvalidOperationException($"[Error] A parameter object '{parameterObject["name"]}' " +
                    $"has invalid 'hasValue' property value - '{hasValue}'. Valid are 'true' or 'false'");
        }

        string FormatStringByWidth(string text) {
            string[] textParts = text.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            int currentLength = 0;
            string result = string.Empty;
            for (int textPartIndex = 0; textPartIndex < textParts.Length; textPartIndex++) {
                string textPart = textParts[textPartIndex];
                if (currentLength + textPart.Length < ScreenWidthInChars) {
                    currentLength += textPart.Length + 1;
                    result += $"{textPart} ";
                }
                else {
                    currentLength = textPart.Length + 1;
                    result += $"\n{textPart} ";
                }
            }
            return result;
        }

        string FormatNameAndDescription(string name, string description, string shift) {
            string[] descriptionParts = description.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            int shiftLength = shift.Length;
            int currentLength = shiftLength;
            string result = name + new string(' ', shiftLength - name.Length);
            for (int textPartIndex = 0; textPartIndex < descriptionParts.Length; textPartIndex++) {
                string textPart = descriptionParts[textPartIndex];
                if (currentLength + textPart.Length < ScreenWidthInChars) {
                    currentLength += textPart.Length + 1;
                    result += $"{textPart} ";
                }
                else {
                    currentLength = shiftLength + textPart.Length + 1;
                    result += $"\n{shift}{textPart} ";
                }
            }
            return result;
        }

        string FormatExampleByWidth(string example) {
            if (example.Length <= ScreenWidthInChars)
                return example;
            string[] textParts = example.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            for (int commandPartIndex = 0; commandPartIndex < textParts.Length; commandPartIndex++) {
                string commandPart = textParts[commandPartIndex];
                if (commandPart.StartsWith("-") && commandPartIndex != 0)
                    result += "\\\n";
                result += $"{commandPart} ";
            }
            return result.Trim();
        }

        string FormatCommandByWidth(string command) {
            string[] textParts = command.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            int currentLength = 0;
            string result = string.Empty;
            for (int commandPartIndex = 0; commandPartIndex < textParts.Length; commandPartIndex++) {
                string commandPart = textParts[commandPartIndex];
                if (currentLength + commandPart.Length + 1 < ScreenWidthInChars) {
                    currentLength += commandPart.Length + 1;
                    result += $"{commandPart} ";
                }
                else {
                    currentLength = commandPart.Length + 2;
                    result += $"\\\n{commandPart} ";
                }
            }
            return result;
        }
        #endregion (Support methods)
    }
}
