﻿using System;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;
using Newtonsoft.Json;

namespace Yamlarm {
    #region IMultiSerializer
    public interface IMultiSerializer {
        public void SerializeToFile(object theObject, string format, string fileName);

        public object DesializeFromFile(string fileName);

        List<string> SupportedFormats { get; }
    }
    #endregion (IMultiSerializer)

    #region Serializer
    public class MultiSerializer : IMultiSerializer {
        IDeserializer yamlDeserializer;
        ISerializer yamlSerializer;
        List<string> supportedFormats;

        public MultiSerializer() {
            yamlDeserializer = new DeserializerBuilder().Build();
            yamlSerializer = new SerializerBuilder().Build();
            supportedFormats = new List<string>(new string[] { "json", "yaml" });
        }

        public List<string> SupportedFormats { get { return supportedFormats; } }

        public void SerializeToFile(object theObject, string format, string fileName) {
            switch (format) {
                case "json":
                    SerializeToJsonFile(theObject, fileName);
                    return;
                case "yaml":
                    SerializeToYamlFile(theObject, fileName);
                    return;
                default:
                    throw new InvalidOperationException($"[Error] Error serializing object to a file with format '{format}' - it is not supported.");
            }
        }

        public object DesializeFromFile(string fileName) {
            string extension = Path.GetExtension(fileName).Substring(1);
            switch (extension) {
                case "json":
                    return DeserializeJsonFile(fileName);
                case "yaml":
                    return DeserializeYamlFile(fileName);
                default:
                    throw new InvalidOperationException($"[Error] Error deserializing a file with extension '{extension}' - the format is not supported.");
            }
        }

        #region YAML
        protected void SerializeToYamlFile(object theObject, string fileName) {
            File.AppendAllText(fileName, yamlSerializer.Serialize(theObject));
        }

        protected object DeserializeYamlFile(string fileName) {
            object result;
            using (StreamReader reader = File.OpenText(fileName)) {
                result = yamlDeserializer.Deserialize<object>(reader);
            }
            return result;
        }
        #endregion (YAML)

        #region JSON
        protected object DeserializeJsonFile(string fileName) {
            return JsonConvert.DeserializeObject<object>(File.ReadAllText(fileName), new CustomConverter());
        }

        protected void SerializeToJsonFile(object theObject, string fileName) {
            File.AppendAllText(fileName, JsonConvert.SerializeObject(theObject, Formatting.Indented));
        }
        #endregion (JSON)
    }
    #endregion (Serializer)

    #region CustomJSONConverter
    public class CustomConverter : JsonConverter {
        JsonSerializer standardSerializer;

        public CustomConverter() {
            standardSerializer = new JsonSerializer();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            throw new InvalidOperationException("[Error] CustomConverter is not ment to be used for Serialization - only for deserialization");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            //object val = reader.Value;
            switch (reader.TokenType) {
                case JsonToken.Comment:
                    reader.Skip();
                    return string.Empty;
                case JsonToken.None:
                    return string.Empty;
                case JsonToken.Null:
                    reader.Read();
                    return string.Empty;
                case JsonToken.Undefined:
                    reader.Skip();
                    return string.Empty;
                case JsonToken.StartArray:
                    List<object> array = new List<object>();
                    reader.Read();
                    while (reader.TokenType != JsonToken.EndArray)
                        array.Add(serializer.Deserialize<object>(reader));
                    reader.Read();
                    return array;
                case JsonToken.StartObject:
                    Dictionary<object, object> dict = new Dictionary<object, object>();
                    reader.Read();
                    while (reader.TokenType != JsonToken.EndObject) {
                        string propertyName = serializer.Deserialize<string>(reader);
                        object propertyValue = serializer.Deserialize<object>(reader);
                        dict.Add(propertyName, propertyValue);
                    }
                    reader.Read();
                    return dict;
                case JsonToken.PropertyName:
                    string name = reader.Value.ToString();
                    reader.Read();
                    return name;
                case JsonToken.Raw:
                    break;
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.String:
                case JsonToken.Boolean:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    object value = reader.Value;
                    reader.Read();
                    return value;
                case JsonToken.EndObject:
                    throw new InvalidOperationException("[Error] Custom converter error: should not come to ReadJson method with EndObject token type");
                case JsonToken.EndArray:
                    throw new InvalidOperationException("[Error] Custom converter error: should not come to ReadJson method with EndArray token type");
                case JsonToken.StartConstructor:
                    throw new InvalidOperationException("[Error] Custom converter error: come to ReadJson method with unknows token type - StartConstructor");
                case JsonToken.EndConstructor:
                    throw new InvalidOperationException("[Error] Custom converter error: come to ReadJson method with unknows token type - EndConstructor");
                default:
                    throw new InvalidOperationException($"[Error] Custom converter error: come to ReadJson method with unhandled token type - {reader.TokenType}");
            }
            throw new InvalidOperationException($"[Error] Custom converter error: ended ReadJson without handling token type - {reader.TokenType}");
        }

        public override bool CanRead { get { return true; } }

        public override bool CanConvert(Type objectType) {
            return true;
        }
    }
    #endregion (CustomJSONConverter)
}
