# YamlArm

## Description

A command line tool that provides an ability to deploy Azure ARM Templates from several files with supported formats. Has additional commands for converting template files between supported formats, creating resulting deployment file without any further actions and checking templates without deployment. Supported formats: JSON, YAML.

## Prerequisite
A .Net 5 tool. Uses Azure CLI.

## File formats
The tool supports two file formats for now: JSON and YAML.
The tool determines a file's formats by its extension (part of its name after the last '.' symbol). Extensions are:
- ```.json``` for JSON format
- ```.yaml``` and ```.yml``` for YAML format
After detecting known format the tool reads its content and tries to deserialize it. If it fails it will output a warning and move on skipping it.

## File structure
ARM Template file contains several obligatory sections. Among them are:
* ```$schema``` with the schema version
* ```resources``` with an array of data describing deployed resources
The tool provides an ability to combine several source files into a final ARM Template JSON file. It can successfully combine one file with the specified obligatory sections - lets call it a ```main file``` - and any number of files that contain single object or array of objects - lets call them ```resource files```. The tool processed only files with a supported format.

## Avaliable commands
* convert - to convert template files from JSON to YAML or vice versa
* combine - to generate a resulting single template JSON file from a folder with template files in JSON or YAML formats
* check - to generate a resulting template and check it using specified Azure Subscripton and Resource Group and with specified parameters and mode
* deploy - to generate a resulting template and deploy it to a specified Azure Subscripton and Resource Group and with specified parameters and mode

## Command syntax
General syntax:
```
yamlarm <command> [Options] path
```
```path``` - a folder, absolute or relative, where tool should search for files to complete the command with.

Common options:
```
-r, --recurse                   apply to all subfolder of the path recursively 
-d, --debug                     more verbose command output
```
Each command also has its specific options

## Convert
This command tells the tool to convert all files from a specified folder with formats different from a specified one into that format. Converted files may be put into an output folder if such is specified. The source files may be moved to a subfolder (of their root folder) with ```before_conversion``` or specified name if a specific option is used.
Supported formats:
* JSON
* YAML

Default behaviour is to convert all JSON files inside a specified folder into a YAML files with no additional actions.
```
yamlarm convert [Options] path
```
Command-specific options:
```
-f string, --format string      a format to convert files to, 
                                valid string values are 'yaml', 'json',
                                default is 'yaml'
-m [string], --move [string]    move a source files to an absolute folder (if an
                                absolute path is specified), to a subfolder of  
                                the 'path' (if a relative path is specified) or to 
                                the 'before_conversion' folder (if no value specified)
-o string, --output string      a folder where to store converted files,
                                default is the path
```
Example:
```
yamlarm convert -f yaml -m source -o result template_folder/deployment1
yamlarm convert --format json --recurse --output d:\armt\json c:\armt\yaml
```

## Combine
This command tells the tool to take all files from a specified folder with supported formats and combine them into a single ARM Template JSON file. This command will succeed only if the specified folder (plus subfolders if recursion is specified) contains one ```main file``` (see *File structure* section above). The tool takes root object (or objects if there is an array in the file) from ```resource files``` and adds them to the ```resource``` array of the ```main file```. If the resulting ```resource``` array is empty the error is returned.

Default behaviour is to combine all files with supported formats from a specified folder into a single JSON file that is put into its ```deployment_info``` subfolder.
```
yamlarm combine [Options] path
```
Command-specific options:
```
-o string, --output string      a subfolder where to store the result JSON file
```
Example:
```
yamlarm combine --debug --output result deployment_files/current
yamlarm combine -r c:\projects\templates\1
```

## Check


Default behaviour is combine all files with supported formats from a specified folder into a single JSON file that is put into its ```deployment_info``` subfolder and check it storing logs to the same subfolder.
```
yamlarm check [Options] path
```
Command-specific options:
```
-o string, --output string          a subfolder where to store the result JSON file
                                    and command logs
-s string, --subscription string    an Azure Subscription to use
-g string, --resourcegroup string   an Azure Resource Group to use
-p string, --parameters string      a parameters string with syntax:
                                    'param1=value1;param2=value2;...'
```
Example:
```
```

## Deploy
This command tells the tool to do all the same as the ```combine``` command does, but also to deploy the result ARM Template JSON file using the ```az deployment group create``` Azure CLI command (or just check with ```az deployment group validate``` command) and specified Azure Subscription, Resource Group and parameters if specified. If Resouce Group and/or Subscription are not specified then the default values set using Azure CLI will be used. If no default values are set then an Azure CLI error will be returned.

Default behaviour is combine all files with supported formats from a specified folder into a single JSON file that is put into its ```deployment_info``` subfolder and deploy it with an ```Incremental``` deployment mode storing logs to the same subfolder.
```
yamlarm deploy [Options] path
```
Command-specific options:
```
-o string, --output string          a subfolder where to store the result 
                                    JSON file and command logs
-s string, --subscription string    an Azure subscription to use
-g string, --resourcegroup string   an Azure Resource Group to use
-p string, --parameters string      a parameters string or syntax:
                                    'param1=value1;param2=value2;...'
-v [string], --validate [string]    if included or set to 'true' then run 
                                    just validation command without any
                                    real deployment, valud values are 'true'
                                    and 'false', default is 'false'
-m string, --mode string            a deployment mode to use, valid values
                                    are 'incremental' and 'сomplete', 
                                    default is 'incremental'
```
Example:
```
yamlarm deploy -v -s MySubscription -g MyResourceGroup -p 'VN_NAME=my-vn;NSG_NAME=my-nsg' mytemplates

yamlarm deploy \
--subscription some-subscription \
--resourcegroup some-rg \
--parameters 'VN_NAME=my-vn;NSG_NAME=my-nsg' \
--output logs \
--mode complete \
dev_state
```